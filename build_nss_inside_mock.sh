#!/bin/bash
# Build package inside mock


# Ignore these warnings
#   Installing /builddir/build/RPMS
#   error: not an rpm package
#   error: /builddir/build/RPMS cannot be installed
cd /builddir/build/RPMS/

# build a client package and install all the produced rpm's
default="nss"
pkg=${pkg:-$default}

# build and install the nss packages
rpmbuild --rebuild /tmp/${pkg}-*.src.rpm
cd /builddir/build/RPMS
rpm --force -ihv *.rpm

