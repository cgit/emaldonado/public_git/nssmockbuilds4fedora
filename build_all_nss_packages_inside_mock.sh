#!/bin/bash
# Build a package inside mock


# Ignore these warnings
#   Installing /builddir/build/RPMS
#   error: not an rpm package
#   error: /builddir/build/RPMS cannot be installed
cd /builddir/build/RPMS/

# build a client package and install all the produced rpm's
default="nss"
pkg=${pkg:-$default}

# build and install the nss packages
rpmbuild --rebuild /tmp/${pkg}-*.src.rpm
cd /builddir/build/RPMS
all_rpms=`ls *.rpm`
new_rpms=()
# build up the array of the ones not installed yet
for file in $all_rpms; do
    notInstalled=`rpm -q ${file} | grep "not installed"`
    if [ -n "${notInstalled}" ]; then
        new_rpms+=($file)
    fi
done
# Must use force in incremental builds as sometines an older version is still 
# installed and we end up with conflicts. In real life this shoudn't happen
# and neither have I seen this on koji builds.
rpm --force -ihv *.rpm

# tLen=${#NAMESERVERS[@]}
#if [ ${#new_rpms[@]} -gt 0 ]; then 
#    rpm --force -ihv ${new_rpms[@]}
#    echo "Force-installed: ${new_rpms[@]}"; 
#else 
#    rpm --force -ihv *.rpm
#    echo "Force-installed: ${all_rpms}"; 
#fi

