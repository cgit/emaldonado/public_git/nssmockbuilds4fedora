#!/bin/bash

# checks out nspr, nss-util, nss-softokn, nss, and client pacakges
# creates their srpms and copies them where build-nss.py expects them
# Usage: prepare-all-srpms [-b branch], defaults to master
#
# TODO: Pass the package names as arguments

checkout_pkgs() {
  if [ -n "$2" ] ; then
    OPT_BRANCH="--branch $2"
  else
    OPT_BRANCH=""
  fi
  for p in $1; do
    fedpkg clone $p ${OPT_BRANCH} -a
  done
}

remove_pkgs() {
  for p in $1; do
    rm -fr $p
  done
}

#----------------------------------------------------------
nssPkgs="nspr nss-util nss-softokn nss"
clientPkgs=""

# default
branch=master
while getopts b:s: opt
do
  case $opt in
  b) export branch="$OPTARG";;
  ?) printf "Usage: %s: [-b branch]\n defaults to master"  $0
     exit 2;;
  esac
done

echo "branch = ${branch}"

remove_pkgs "${nssPkgs}"
remove_pkgs "${clientPkgs}"
checkout_pkgs "${nssPkgs}" "${branch}"
checkout_pkgs "${clientPkgs}" "${branch}"

