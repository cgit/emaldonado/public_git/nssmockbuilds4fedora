#!/bin/bash
# Build the nss package set and some nss clients inside mock

dist=rawhide
arch=x86_64

# Ignore these warnings
#   Installing /builddir/build/RPMS
#   error: not an rpm package
#   error: /builddir/build/RPMS cannot be installed
cd /builddir/build/RPMS/

# build and install the nss packages
nss_pkgs="nspr nss-util nss-softokn nss"
NSS_PKGS=${NSS_PKGS:-$nss_pkgs}

client_pkgs=""
CLIENT_PKGS=${CLIENT_PKGS:-$client_pkgs}

# build and install the nss packages
for pkg in $NSS_PKGS; do
    rpmbuild --rebuild /tmp/${pkg}-*.src.rpm
    package=`rpm -qp /tmp/${pkg}-*.src.rpm`
    notInstalled=`rpm -q $package | grep "not installed"`
    if [ -n "${notInstalled}" ]; then
        rpm -ihv /builddir/build/RPMS/${pkg}-*
    fi
done
# build the client packages
#for pkg in $CLIENT_PKGS; do
#    rpmbuild --rebuild /tmp/${pkg}-*.src.rpm
#done

