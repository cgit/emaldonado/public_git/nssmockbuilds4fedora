#!/bin/bash
# Build the nss package set and some nss clients inside mock
# 
# 1. prepare for building: initialize the mock environment and
# install # packages we will need later when building nss
# 2. create a mock shell to build nss packages
# 3. build the nss related packages inside mock installing
#    the newly built ones as we go along
#
# This script is meant to be invoked by build_nss.py

default_dist=rawhide
dist=${dist:-$default_dist}
default_arch=x86_64
arch=${arch:-$default_arch}
#arch2=[ "${arch}" = "x86_64" ] && x86_64 || i386
if [ "${arch}" = "x86_64" ]; then
arch2=x86_64
else
arch2=i386
fi

# prepare for build
# initialize the mock environment and install
# packages we will need later when building nss
mock -r fedora-${dist}-${arch} --init

nss_pkgs="nspr nss-util nss-softokn nss"
nss_pkgs=${nss_pkgs:-$nss_pkgs}

# listed in rough order of importance and dependencies
all_clients=""
clients=${clients:-$all_clients}
workdir=`pwd`
for pkg in ${nss_pkgs}; do
    fedpkg srpm
    mock -r fedora-${dist}-${arch2} --copyin ${pkg}/${pkg}-*.src.rpm /tmp
done
# repeat for client packages
for client in ${clients}; do
    fedpkg srpm
    mock -r fedora-${dist}-${arch2} --copyin ${client}/${client}-*.src.rpm /tmp
done

# copy the build script into mock
mock -r fedora-${dist}-${arch} --copyin ./build_nss_inside_mock.sh /usr/bin

# create a mock shell and execute a build script there package by package
# Previously built and installed packages upon which current one depends are kept
# in the buildroot with via the --no-clean and --no-cleanup-after options.
# This guarantees that all the srpm dependencies will be met.
for pkg in $nss_pkgs; do
    mock -r fedora-${dist}-i386 --installdeps ${pkg}/${pkg}-*.src.rpm
    mock -r fedora-${dist}-i386 --shell pkg=${pkg} --no-clean --no-cleanup-after build_nss_inside_mock.sh
done

for client in $clients; do
    mock -r fedora-${dist}-${arch2} --installdeps ${client}/${client}-*.src.rpm
    mock -r fedora-${dist}-${arch2} --shell --no-clean --no-cleanup-after pkg=${client} build_nss_inside_mock.sh
done

#----------------------------------------------------------------------------------------
# If needed for debugging we can shell into mock and build and install package by package
#----------------------------------------------------------------------------------------

