#!/bin/bash

# checks out nspr, nss-util, nss-softokn, nss, and client pacakges
# creates their srpms and copies them where build-nss.py expects them
# Usage: prepare-all-srpms [-b branch], defaults to master
#
# TODO: Pass the package names as arguments

remove_pkgs() {
  for p in $1; do
    rm -fr $p
  done
}

srpm_pkgs() {
  if [ -n "$2" ]; then
    export OPTION_BRANCH="--branch $2"
  else
    export OPTION_BRANCH=""
  fi
  for p in $1; do
    pushd $p
    fedpkg srpm
    cp -p $p-*.src.rpm ../packages/SRPMS
    popd
  done
}

checkout_and_srpm_pkgs() {
  if [ -n "$2" ]; then
    export OPTION_BRANCH="--branch $2"
  else
    export OPTION_BRANCH=""
  fi
  for p in $1; do
      fedpkg clone $p ${OPTION_BRANCH} --anonymous
    pushd $p
    fedpkg srpm
    cp -p $p-*.src.rpm ../packages/SRPMS
    popd
  done
}

#----------------------------------------------------------
nssPkgs="nspr nss-util nss-softokn nss"
clientPkgs=""

# default
branch=master
while getopts b:s: opt
do
  case $opt in
  b) export branch="$OPTARG";;
  ?) printf "Usage: %s: [-b branch]\n defaults to master"  $0
     exit 2;;
  esac
done

echo "branch = ${branch}"

if [ -e packages/SRPMS ]; then
  rm -rf packages/SRPMS
fi
mkdir -p packages/SRPMS

remove_pkgs "${nssPkgs}"
remove_pkgs "${clientPkgs}"
checkout_and_srpm_pkgs "${nssPkgs}" "${branch}"
checkout_and_srpm_pkgs "${clientPkgs}" "${branch}"

