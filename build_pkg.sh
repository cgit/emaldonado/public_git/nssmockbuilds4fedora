#!/bin/sh

set -x 

DIR=$(pwd)

XPKG_CMD=$1
PKG=$2
rm -rf $DIR/$PKG
echo check out source from fedora or rhel. We need the spec file
$XPKG_CMD clone -a $PKG

echo download the source from the master branch
cd $DIR/$PKG
XPKG_CMD switch-branch rawhide
XPKG_CMD srpm

if [ ! -d ../packages/SRPMS ]; then
    mkdir -p ../packages/SRPMS 
fi

cp $PKG-*.src.rpm ../packages/SRPMS
