all: use-defaults

use-defaults: prepare
	(export dist=rawhide; export arch=x86_64; \
	export nss_pkgs="nspr nss-util nss-softokn nss"; \
	export clients=""; \
	sh ./build_nss.sh)

build-for-rawhide-i386: prepare
	(dist=rawhide arch=i386 \
	export nss_pkgs="nspr nss-util nss-softokn nss"; export clients=""; ./build_nss.sh)

build-for-rawhide-x86_64: prepare
	(dist=rawhide arch=x86_64 \
	export nss_pkgs="nspr nss-util nss-softokn nss"; export clients=""; ./build_nss.sh)

prepare:
	(export clients=""; ./checkout-all-packages.sh)
	(export clients=""; ./prepare-all-srpms.sh)
